/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


package institute;
import java.io.*;
import java.util. *;
/**
 *
 * @author Henry
 */
public class Institute {
    private String insituteName;
    private List<Department> department;

    public Institute(String insituteName, List<Department> department) {
        this.insituteName = insituteName;
        this.department = department;
    }

    public String getInsituteName() {
        return insituteName;
    }

    public List<Department> getDepartment() {
        return department;
    }
    
    
}
